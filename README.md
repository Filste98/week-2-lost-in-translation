# LOST IN TRANSLATION APP


## Background
This application consists of a login page, a profile page and a translator page. There's also a navbar to navigate the user when logged in.

### Login page
The user can input a username that will create a user in an api. The user's data will be remembered.

### Profile page
Displays the user's last 10 translations. It also includes a logout button and a clear history button.

### Translator page
Has an input field where the user can type in any sentence and it will be translated to american signlanguage in an output field.

## Usage
Run npm install from the root of the project in a terminal in Visual Studio Code.
```bash
npm install
```
To start a local host you can use npm start in the root of the project in the terminal. Your web-browser should open up with the login-page on your first try. 
```bash
npm start
```

## Author
Gitlab: Filste98
Gitlab link: https://gitlab.com/Filste98


## License
[MIT](https://choosealicense.com/licenses/mit/)
