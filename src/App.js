import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Login from './views/Login';
import Translator from './views/Translator';
import Profile from './views/Profile';
import NavBar from './components/Navbar/NavBar';

function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <NavBar />
        <Routes>
          <Route path="/" element={ <Login />} />
          <Route path="translator" element={ <Translator />} />
          <Route path="profile" element={ <Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
