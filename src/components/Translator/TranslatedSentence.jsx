
const TranslatedSentence = ({ sentenceArray }) => {
    if(!sentenceArray){
        return;
    }

    const signsList = sentenceArray.map((character, index) => 
        (character === " " && <div key={ index + "-" + character } className="SpacingDiv" />) || 
        (character !== " " && <img key={ index + "-" + character } src={process.env.PUBLIC_URL + "individual_signs/" + character + ".png"} />)
    )
    return <div>
        { signsList }
    </div>
}

export default TranslatedSentence;