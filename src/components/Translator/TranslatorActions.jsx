
import {useState} from 'react';
import { translationAdd } from "../../api/translation";
import { useUser } from '../../context/UserContext';
import "./TranslatorActions.css"
import TranslatedSentence from './TranslatedSentence';
import { storageSave } from '../../utils/storage';
import { STORAGE_KEY_USER } from '../../cons/storageKeys';

const TranslatorActions = () => {

    const { user, setUser } = useUser();
    const [translationInput, setValue] = useState("");
    const [translatedOutput, setTranslatedOutput] = useState("");

    const translateInputClicked = async () => {
        try {
            if(translationInput.trim() !== ""){
                //Adds translation to API
                const [error, updatedUser] = await translationAdd(user, translationInput);
                if(error !== null){
                    //BAD:
                    return;
                }
                //Keep UI state and Server state in sync
                storageSave(STORAGE_KEY_USER, updatedUser)
                //Update context state
                setUser(updatedUser);
    
                //Sets output:
                const sentenceArray = translationInput.split("");
                setTranslatedOutput(sentenceArray);
            }
        } catch (error) {
            throw new Error("Something went wrong. Couldn't translate sentence.")
        }
    }

    return (
        <>
        <div className="TranslatorContainer">
            <div className="InputBox">
                <span className="KeyboardIcon">
                    ⌨
                </span>
                <input
                    className="InputField"
                    type="text"
                    value={translationInput}
                    onChange={(event) => {setValue(event.target.value)}}
                    placeholder="Translate something!"
                />
                <button
                    type="submit"
                    className="SendInputBtn"
                    onClick={translateInputClicked}>
                    ✔
                </button>
            </div>

            <div className="TranslatedTextBox">
                <TranslatedSentence sentenceArray={translatedOutput}/>
            </div>
        </div>
        </>
    );
}

export default TranslatorActions;