import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";
import "./ProfileList.css"

const ProfileTranslationHistory = ({ translations }) => {

    const translationsList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={index + "-" + translation} translation={translation} />
    );

    return (
        <section>
            <h4>Your last translations</h4>
            {(translationsList.length < 1 && <p>Oh dear! You haven't translated anything yet!</p>)}
            <ol>{translationsList.slice(Math.max(translationsList.length - 10, 0))}</ol>
        </section>
    );
}

export default ProfileTranslationHistory;