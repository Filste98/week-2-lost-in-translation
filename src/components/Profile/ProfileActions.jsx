import { translationClearHistory } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../cons/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";
import "./ProfileActions.css"

const ProfileActions = () => {

    
    const { user, setUser } = useUser();

    const handleLogoutClick = () => {
        if( window.confirm("Are you sure?") ) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null);
        }
    }

    const handleClearHistoryClick = async () => {
        if(!window.confirm("Are you sure?\nThis can not be undone!") ) {
           return; 
        }

        const [ clearError ] = await translationClearHistory(user.id);
        
        if (clearError !== null){
            return;
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
    }

    return (
        <>
        <div className="ProfileButtons">
            <button id="ClearHistoryButton" onClick={ handleClearHistoryClick }>Clear history</button>
            <button id="LogoutButton" onClick={ handleLogoutClick }>Logout</button>
        </div>
        </>
    );
}

export default ProfileActions;