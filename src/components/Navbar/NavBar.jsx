import "./NavBar.css"
import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext";

const NavBar = () => {

    const { user } = useUser();

    return (
        <nav className="NavBar">
            <span className="NavTitle">
                Lost in Translation
            </span>

            {user !== null &&
                <span className="NavLinks">
                    <NavLink id="TranslatorLink" to="/translator">🖖 Translator</NavLink>
                    <NavLink id="ProfileLink" to="/profile">👤 Profile</NavLink>
                </span>
            }
        </nav>
    );
}

export default NavBar;