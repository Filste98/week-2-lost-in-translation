import 'animate.css';
import "./MainTitle.css"

function MainTitle() {

    return (
        <div className="MainTitle">
            <div className="Avatar">
                <img src="https://cdn-icons-png.flaticon.com/512/1126/1126923.png?w=740&t=st=1660823396~exp=1660823996~hmac=2bbe17df37f0d7d405ce7c72d8ae43ffd200ca2b6b616b50269106fa5147c391" alt="Websites avatar" />
            </div>
            <div className="TitleText animate__animated animate__backInRight">
                <h1>Lost in Translation</h1>
                <h2>Get started</h2>
            </div>
        </div>
    );
}

export default MainTitle;