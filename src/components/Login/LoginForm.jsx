import { useState, useEffect } from "react";
import { useForm } from "react-hook-form"
import { loginUser } from "../../api/user"
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom"
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../cons/storageKeys";
import "../InputField.css"

const usernameConfig = {
    required: true,
    minLength: 2
}

const LoginForm = () => {

    //Hooks
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    //Local State
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    //Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate("profile");
        }

    }, [user, navigate])  //Empty Deps - Only run once

    //Event Handlers
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);
        if (error !== null) {
            setApiError(error);
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse);
            setUser(userResponse);
        }
        setLoading(false);
    };

    //Render Functions
    const errorMessage = (() => {
        if (!errors.username) {
            return null;
        }
        if (errors.username && errors.username.type === "required") {
            return <span>Username is required</span>
        }
        if (errors.username && errors.username.type === "minLength") {
            return <span>Username is too short (min 2 characters)</span>
        }
    })()


    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="InputBox">
                    <span className="KeyboardIcon">
                        ⌨
                    </span>
                    <input
                        className="InputField"
                        type="text"
                        placeholder="What's your name?"
                        {...register("username", usernameConfig)}
                    />
                    <button
                        type="submit"
                        disabled={loading}
                        className="SendInputBtn">
                        ✔
                    </button>
                </div>
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
                {errorMessage}
            </form>
        </>
    );
}

export default LoginForm;