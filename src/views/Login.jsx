import LoginForm from "../components/Login/LoginForm"
import MainTitle from "../components/Login/MainTitle"


const Login = () => {
    return (
        <>
            <MainTitle />
            <LoginForm />
        </>
    )
}
export default Login