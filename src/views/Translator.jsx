import TranslatorActions from "../components/Translator/TranslatorActions"
import withAuth from "../hoc/withAuth"

const Translator = () => {

    return (
        <>
            <TranslatorActions />
        </>
    )
}
export default withAuth(Translator)